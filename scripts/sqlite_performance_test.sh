#!/bin/bash

# Remove quotes
sed 's/\"//g' ../torrents.csv > torrents_removed_quotes.csv


sqlite3 -batch test.db <<"EOF"
create table torrents(
  "infohash" TEXT,
  "name" TEXT,
  "size_bytes" INTEGER,
  "created_unix" INTEGER,
  "seeders" INTEGER,
  "leechers" INTEGER,
  "completed" INTEGER,
  "scraped_date" INTEGER
);
.separator ";"
.import torrents_removed_quotes.csv torrents
UPDATE torrents SET completed=NULL WHERE completed = '';
EOF

echo "Ripgrep time for pearl jam"
time rg -i "pearl.*jam" ../torrents.csv > /dev/null

echo "Sqlite3 time"
time sqlite3 test.db "select * from torrents where name like '%pearl%jam%' limit 10" > /dev/null

echo "Ripgrep time for avengers infinity"
time rg -i "avengers.*infinity" ../torrents.csv > /dev/null

echo "Sqlite3 time"
time sqlite3 test.db "select * from torrents where name like '%avengers%infinity%' limit 10" > /dev/null

echo "Ripgrep time for w"
time rg -i "w" ../torrents.csv > /dev/null

echo "Sqlite3 time"
time sqlite3 test.db "select * from torrents where name like '%w%' limit 10" > /dev/null

echo "Ripgrep time for prestige 2006"
time rg -i "prestige.*2006" ../torrents.csv > /dev/null

echo "Sqlite3 time"
time sqlite3 test.db "select * from torrents where name like '%prestige%2006%'  limit 10" > /dev/null

echo "Ripgrep time for why"
time rg -i "w" ../torrents.csv > /dev/null

echo "Sqlite3 time"
time sqlite3 test.db "select * from torrents where name like '%why%' limit 10" > /dev/null

rm test.db
rm torrents_removed_quotes.csv